const data = require("../data");
const problem1 = require("../problem1");
// Q1 Find all users who are interested in playing video games.

function test1(data) {
  return problem1(data);
}

const res = test1(data);
console.log(res);

//Q4 Group users based on their Programming language mentioned in their designation.
const problem4 = (data) => {
  if (
    typeof data == "string" ||
    typeof data == "boolean" ||
    typeof data == "number" ||
    typeof data == "symbol" ||
    data == null
  ) {
    data == null
      ? console.log("Input is null, send a valid input.")
      : console.log(`Input is ${typeof data}, send a valid input`);
    return;
  } else if (data.length == 0) {
    console.log(`Empty data, please send a valid data`);
    return;
  }

  let users = {
    Python: [],
    Javascript: [],
    Golang: [],
  };
  for (let ele in data) {
    if (data[ele].designation.includes("Python")) {
      users.Python.push(ele);
    } else if (data[ele].designation.includes("Javascript")) {
      users.Javascript.push(ele);
    } else if (data[ele].designation.includes("Golang")) {
      users.Golang.push(ele);
    }
  }
  //console.log(users);
  return users;
};

module.exports = problem4;

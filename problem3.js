//Q3 Find all users with masters Degree.
const problem3 = (data) => {
  if (
    typeof data == "string" ||
    typeof data == "boolean" ||
    typeof data == "number" ||
    typeof data == "symbol" ||
    data == null
  ) {
    data == null
      ? console.log("Input is null, send a valid input.")
      : console.log(`Input is ${typeof data}, send a valid input`);
    return;
  } else if (data.length == 0) {
    console.log(`Empty data, please send a valid data`);
    return;
  }

  let users = [];
  for (let ele in data) {
    if (data[ele].qualification == "Masters") {
      console.log(`${ele} has Masters`);
      users.push(ele);
    }
  }
  return users;
};

module.exports = problem3;

// Q1 Find all users who are interested in playing video games.
const problem1 = (data) => {
  if (
    typeof data == "string" ||
    typeof data == "boolean" ||
    typeof data == "number" ||
    typeof data == "symbol" ||
    data == null
  ) {
    data == null
      ? console.log("Input is null, send a valid input.")
      : console.log(`Input is ${typeof data}, send a valid input`);
    return;
  } else if (data.length == 0) {
    console.log(`Empty data, please send a valid data`);
    return;
  }

  let users = [];
  for (let ele in data) {
    let obj = data[ele];
    let interests = obj.interests;
    interests.forEach((element) => {
      if (element.includes("Video Games")) {
        console.log(`${ele} plays Video games`);
        users.push(ele);
      }
    });
  }
  return users;
};

module.exports = problem1;
